package com.example.objectif2tonnes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

public class ChoixHabitudes extends AppCompatActivity {

    CheckBox cbhab1, cbhab2, cbhab3, cbhab4, cbhab5;
    Button submit;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_habitudes);

        cbhab1 = findViewById(R.id.hab1);
        cbhab2 = findViewById(R.id.hab2);
        cbhab3 = findViewById(R.id.hab3);
        cbhab4 = findViewById(R.id.hab4);
        cbhab5 = findViewById(R.id.hab5);
        submit = findViewById(R.id.okhabitudes);

        //mets une couleur au texte des checkbox cochées

        cbhab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbhab1.isChecked()){
                    cbhab1.setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    cbhab1.setTextColor(getResources().getColor(R.color.black));
                }

            }
        });

        cbhab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbhab2.isChecked()){
                    cbhab2.setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    cbhab2.setTextColor(getResources().getColor(R.color.black));
                }

            }
        });

        cbhab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbhab3.isChecked()){
                    cbhab3.setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    cbhab3.setTextColor(getResources().getColor(R.color.black));
                }

            }
        });

        cbhab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbhab4.isChecked()){
                    cbhab4.setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    cbhab4.setTextColor(getResources().getColor(R.color.black));
                }

            }
        });

        cbhab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbhab5.isChecked()){
                    cbhab5.setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    cbhab5.setTextColor(getResources().getColor(R.color.black));
                }

            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sp = getSharedPreferences("memHab", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();

                Intent messageVersActivity2 = new Intent();

                if (cbhab1.isChecked()){
                    String message1 = cbhab1.getText().toString();
                    editor.putString("hab1", message1);
                    messageVersActivity2.putExtra("hab1", message1);
                }
                if (cbhab2.isChecked()){
                    String message1 = cbhab2.getText().toString();
                    editor.putString("hab2", message1);
                    messageVersActivity2.putExtra("hab2", message1);
                }
                if (cbhab3.isChecked()){
                    String message1 = cbhab3.getText().toString();
                    editor.putString("hab3", message1);
                    messageVersActivity2.putExtra("hab3", message1);
                }
                if (cbhab4.isChecked()){
                    String message1 = cbhab4.getText().toString();
                    editor.putString("hab4", message1);
                    messageVersActivity2.putExtra("hab4", message1);
                }
                if (cbhab5.isChecked()){
                    String message1 = cbhab5.getText().toString();
                    editor.putString("hab5", message1);
                    messageVersActivity2.putExtra("hab5", message1);
                }

                //Intent messageVersActivity = new Intent();
                //messageVersActivity.setClass(getApplicationContext(), Calendrier.class);

                editor.apply();

                messageVersActivity2.setClass(getApplicationContext(), com.example.applicw.Calendrier.class);
                startActivity(messageVersActivity2);

                //startActivity(messageVersActivity);

                finish();

            }
        });




    }

    /** public void changerCalendar(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), Calendrier.class);
        messageVersActivity.putExtra("message1", s);
        startActivity(messageVersActivity);
        finish(); */


        }
