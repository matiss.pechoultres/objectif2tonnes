package com.example.objectif2tonnes;

import java.util.Iterator;
import java.util.Vector;

public class Quiz {

    private Vector<com.example.applicw.Habitude> lesHabitudes;


    public Quiz() {
        // 0 si jamais été prise, 1 si en cours, 2 si réalisée pednat une semaine
        lesHabitudes = new Vector<com.example.applicw.Habitude>();
        lesHabitudes.add(new com.example.applicw.Habitude(
                "Privilégier le covoiturage",
                0,
                "Diminue émissions CO2 blabla",
                "Transport"
                ));

        lesHabitudes = new Vector<com.example.applicw.Habitude>();
        lesHabitudes.add(new com.example.applicw.Habitude(
                "Privilégier le vélo",
                0,
                "blabla",
                "Transport"
        ));

        lesHabitudes = new Vector<com.example.applicw.Habitude>();
        lesHabitudes.add(new com.example.applicw.Habitude(
                "Consommer des produits locaux",
                0,
                "blabla",
                "Alimentation"
        ));

        lesHabitudes = new Vector<com.example.applicw.Habitude>();
        lesHabitudes.add(new com.example.applicw.Habitude(
                "Préférer le train à l'avion",
                0,
                "blabla",
                "Transport"
        ));

    }

    /**
     * Fournit un itérateur sur les questions du quiz
     *
     * @return itérateur sur les questions du quiz
     */
    public Iterator<com.example.applicw.Habitude> getHabitudesIterator() {
        return lesHabitudes.iterator();
    }
}