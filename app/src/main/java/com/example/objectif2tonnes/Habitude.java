package com.example.objectif2tonnes;


public class Habitude {
    // habitude proposée
    private String habitude;

    // état de l'habitude : adoptée ou non
    private int état;

    // description - détails
    private String détails;

    // lien à la catégorie
    private String catégorie;


    public Habitude(String habitude, int état, String détails, String catégorie) {
        this.habitude = habitude;
        this.état = état;
        this.détails = détails;
        this.catégorie = catégorie;
    }

    public int déjàadoptée(String habitude) {
        return état;
    }

    public String getHabitude() {
        return habitude;
    }

    public String getCatégorie() {
        return catégorie;
    }

    public String getDétails() { return détails; }

}
