package com.example.objectif2tonnes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class PageBravo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_bravo);

        SharedPreferences sp = getSharedPreferences("membilancarbone", Context.MODE_PRIVATE);
        float ancienbilancarbone = -1;
        if (sp.contains("bilancarbone")){
            ancienbilancarbone = sp.getFloat("bilancarbone", -1);}

        TextView tvMessage = findViewById(R.id.textView2);
        String message = "Félicitation ! Vous avez adopté de nouvelles habitudes !" + ".\n" +"Votre ancien bilan carbone était de " + ancienbilancarbone + " CO2 équivalents";
        tvMessage.setText(message);
    }

    public void envoyer(View view) {
        Intent versAppSMS = new Intent(Intent.ACTION_SENDTO);
        versAppSMS.setData(Uri.parse("smsto:"));
        versAppSMS.putExtra("sms_body","Salut ! J'ai pris de nouvelles habitudes pour réduire mon bilan carbone ! Rejoins moi sur MaSuperApplication !");
        Intent choixAppSMS = Intent.createChooser(versAppSMS, "Avec quelle app ?");
        startActivity(choixAppSMS);
    }

    public void verspageAccueil(View view) {
        Intent versAccueil = new Intent();
        versAccueil.setClass(this, MainActivity.class);
        EditText et = findViewById(R.id.editTextTextPersonName);
        String bilan = et.getText().toString();
        versAccueil.putExtra("bilan", bilan);
        startActivity(versAccueil);
    }
}