package com.example.objectif2tonnes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class Calendrier extends AppCompatActivity {

    ConstraintLayout calendrier;
    String nom_habitude;
    SharedPreferences sp;
    Long nb_sec_actuel;
    Long nb_sec_debut;
    String ts;                      //pour le test
    int nb_jours_habitude = 10;
    int duree_jour = 60*60*24;
    int duree_habitude = 60*60*24*nb_jours_habitude;
    Button j1 = findViewById(R.id.button1);
    Button j2 = findViewById(R.id.button2);
    Button j3 = findViewById(R.id.button3);
    Button j4 = findViewById(R.id.button4);
    Button j5 = findViewById(R.id.button5);
    Button j6 = findViewById(R.id.button6);
    Button j7 = findViewById(R.id.button7);
    Button j8 = findViewById(R.id.button8);
    Button j9 = findViewById(R.id.button9);
    Button j10 = findViewById(R.id.button10);


    /*
    Une habitude est stockée dans le fichier nom_habitude.xml sous la forme :
    {bool_en_cours, date_début, bool_jour1, bool_jour2, bool_jour3, bool_jour4, bool_jour5,
        bool_jour6, bool_jour7, bool_jour8, bool_jour9, bool_jour10}

    On récupère le nom de la tâche,
    On regarde si c'est une tâche en cours,
        Si non, on souhaite la bienvenue et on initialise une nouvelle donnée stockée par le téléphone (sous la forme {'nom habitude', 'temps_départ', '1er jour', ..., '9eme jour'})
        Si oui,
            On regarde à quel bouton correspond le jour en cours
                Si c'est un jour trop lointain, on explique que l'habitude n'a pas été complétée.
                Si le jour est trop lointain, on dit qu'il y a un problème de calendrier.
                Si le jour est celui en cours, on dit qu'il faut revenir dans XX temps.
                Si le jour est convenable, on autorise à cliquer dessus (ça modifie la donnée stockée sur le téléphone)
                    On affiche un message d'encouragement pour la suite.
            Si on clique sur le dernier bouton, on passe à la page de fin.

    Bouton de retour,
    Bouton de ?

    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendrier);
        calendrier = findViewById(R.id.calendrier);


        recuperation_temps();


        Intent habitude = getIntent();
        nom_habitude = habitude.getStringExtra("nom_habitude");

        affichageNomHabitude();



        sp = getSharedPreferences(nom_habitude, Context.MODE_PRIVATE);

        if (sp.contains("debut")){
            couleurs_cases();
            validerJour();
        }
        else {
            creerHabitude();
        }

        
        Button bouton1 = findViewById(R.id.button1);
        bouton1.setText(ts);



    }

    private void validerJour() {
        nb_sec_debut = sp.getLong("debut", 0);
        Long deltaTemps = nb_sec_actuel-nb_sec_debut;
        Long nb_jours = deltaTemps/duree_jour;

        if (deltaTemps <= 0) {
            Toast.makeText(this, "Erreur: vérifier la date dans les paramètres du téléphone...", Toast.LENGTH_SHORT);
        }
        if (deltaTemps > duree_habitude) {
            Toast.makeText(this, "Malheureusement, vous n'avez pas adopté l'habitude. \n Nous vous invitons vivement à réessayer, vous pouvez le faire !", Toast.LENGTH_SHORT);
        }
        if ((deltaTemps > 0) && (deltaTemps <= duree_habitude)) {
            if (nb_jours==1) {
                //j1.setOnClickListener();
            }

        }

    }

    //Récupération du temps (écoulé depuis 1970)
    private void recuperation_temps() {
        nb_sec_actuel = System.currentTimeMillis()/1000;
        ts = nb_sec_actuel.toString();
    }

    private void creerHabitude() {
        SharedPreferences.Editor edit = sp.edit();
        edit.putLong("debut", nb_sec_actuel);
        edit.putBoolean("jour1", false);
        edit.putBoolean("jour2", false);
        edit.putBoolean("jour3", false);
        edit.putBoolean("jour4", false);
        edit.putBoolean("jour5", false);
        edit.putBoolean("jour6", false);
        edit.putBoolean("jour7", false);
        edit.putBoolean("jour8", false);
        edit.putBoolean("jour9", false);
        edit.putBoolean("jour10", false);
        edit.apply();
        Toast.makeText(this, "Bienvenue sur votre nouvelle habitude !", Toast.LENGTH_SHORT).show();
    }

    private void affichageNomHabitude() {
        TextView titre = findViewById(R.id.habitude);
        titre.setText(nom_habitude);
    }

    public void home(View view) {
        Intent home = new Intent();
        home.setClass(getApplicationContext(), MainActivity.class);
        startActivity(home);
        finish();
    }

    public void valider(View view) {
    }
    public void retour(View view) {
    }

    private void couleurs_cases() {
        if (sp.getBoolean("jour1", false)==true) {
            j1.setBackgroundColor(Color.parseColor("#009688"));
        }
        if (sp.getBoolean("jour2", false)==true) {
            j2.setBackgroundColor(Color.parseColor("#009688"));
        }
        if (sp.getBoolean("jour3", false)==true) {
            j3.setBackgroundColor(Color.parseColor("#009688"));
        }
        if (sp.getBoolean("jour4", false)==true) {
            j4.setBackgroundColor(Color.parseColor("#009688"));
        }

        //A compléter!!



    }
}