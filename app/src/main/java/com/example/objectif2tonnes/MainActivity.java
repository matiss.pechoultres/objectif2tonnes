package com.example.objectif2tonnes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        boolean dejavenu = false;
        SharedPreferences sp = getSharedPreferences("memHab", Context.MODE_PRIVATE);
        if (sp.contains("enCours")){
            dejavenu = true;
        }
        Button mesHabitudes = findViewById(R.id.button2);
        Button changercalendar = findViewById(R.id.button3);
        mesHabitudes.setEnabled(dejavenu);
        changercalendar.setEnabled(dejavenu);
    }

    public void changerHabitudes(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), com.example.applicw.ChoixHabitudes.class);
        startActivity(messageVersActivity);
        finish();
    }
    public void changerCalendar(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), com.example.applicw.Calendrier.class);
        startActivity(messageVersActivity);
        finish();
    }
    public void changerCarbone(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), com.example.applicw.CalculCarbone.class);
        startActivity(messageVersActivity);
        finish();
    }
    public void changerBravo(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), com.example.applicw.PageBravo.class);
        startActivity(messageVersActivity);
        finish();
    }

    public void changerMesHab(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), com.example.applicw.ListeHabitudes.class);
        startActivity(messageVersActivity);
        finish();
    }


}