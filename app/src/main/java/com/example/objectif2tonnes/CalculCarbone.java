package com.example.objectif2tonnes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class CalculCarbone extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcul_carbone);


        String url = "https://nosgestesclimat.fr/simulateur/bilan";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }


    public void changerHabitudes(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), ChoixHabitudes.class);
        startActivity(messageVersActivity);
        finish();
    }

    public void verspageChoixHabitudes(View view) {
        Intent versChoixHabitudes = new Intent();
        versChoixHabitudes.setClass(this, ChoixHabitudes.class);
        EditText et = findViewById(R.id.editTextTextPersonName);
        String bilan = et.getText().toString();
        versChoixHabitudes.putExtra("bilan", bilan);

        EditText etbis = findViewById(R.id.editTextTextPersonName);
        Float bilancarbone = Float.parseFloat(etbis.getText().toString());
        SharedPreferences sp = getSharedPreferences("membilancarbone", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat("bilancarbone", bilancarbone);
        editor.apply();

        startActivity(versChoixHabitudes);

    }
}