package com.example.objectif2tonnes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ListeHabitudes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_habitudes);

        /**Intent messageDeChoixHabitudes = getIntent();
        String message = messageDeChoixHabitudes.getStringExtra("message1");*/

        SharedPreferences sp = getSharedPreferences("memHab", Context.MODE_PRIVATE);
        if (sp.contains("enCours")){
            TextView tvMessage = findViewById(R.id.textView4);
            String message = sp.getString("enCours", "a");
            tvMessage.setText(message);
        }
    }

    public void retour(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), MainActivity.class);
        startActivity(messageVersActivity);
        finish();
    }
}